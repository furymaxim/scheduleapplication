package com.furymaxim.schedulenew.models

data class TeacherModel(var teacherName: String, var info: String)