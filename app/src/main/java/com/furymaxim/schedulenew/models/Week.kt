package com.furymaxim.schedulenew.models

data class Week(val number: Int, val year: Int)