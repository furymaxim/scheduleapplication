package com.furymaxim.schedulenew.models

data class MonthModel(val month: HashMap<Int, String>)