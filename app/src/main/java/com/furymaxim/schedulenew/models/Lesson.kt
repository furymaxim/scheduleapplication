package com.furymaxim.schedulenew.models

import android.os.Parcelable
import com.furymaxim.schedulenew.helpers.DateHelper
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Lesson(
    val id: String,
    val start: String,
    val end: String,
    val title: String,
    val teachers: ArrayList<Teacher>,
    val place: String,
    val type: String,
    val text: String
) :
    Parcelable {

    fun getDescription() = title + "\n" + place + "\n" + type + "\n"

    fun getDurationInMinutes(): Int {
        val start = DateHelper.parseTimeUtcToDate(start).time
        val end = DateHelper.parseTimeUtcToDate(end).time
        val minutes = ((end - start) / 1000f / 60f)
        return minutes.toInt()
    }

    fun getStartMinutes(): Int {
        val start = DateHelper.parseTimeUtcToDate(start)
        val calendar = Calendar.getInstance()
        calendar.time = start
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val min = calendar.get(Calendar.MINUTE)
        return hour * 60 + min
    }
}