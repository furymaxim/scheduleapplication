package com.furymaxim.schedulenew.models

data class Events(
    val event: String,
    val time: String,
    val day: String,
    val month: String,
    val year: String
)