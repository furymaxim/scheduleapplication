package com.furymaxim.schedulenew.models

data class YearModel(val year: Int, val months: MonthModel)