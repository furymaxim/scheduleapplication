package com.furymaxim.schedulenew.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Teacher(
    val id: String,
    val name: String,
    val surname: String,
    val fathername: String,
    val position: String
) :
    Parcelable