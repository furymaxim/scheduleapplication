package com.furymaxim.schedulenew.models

data class MyDate(val day: String, val month: String, val year: String)