package com.furymaxim.schedulenew.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.models.TeacherModel
import com.furymaxim.schedulenew.utils.RecyclerViewClickListener

class TeachersRecyclerViewAdapter(
    private val list: MutableList<TeacherModel>,
    private var mOnNewsClickListener: RecyclerViewClickListener
) :
    RecyclerView.Adapter<TeachersRecyclerViewAdapter.TeacherViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): TeacherViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.recycler_item_teacher, viewGroup, false)

        return TeacherViewHolder(v, mOnNewsClickListener)
    }

    override fun onBindViewHolder(viewHolder: TeacherViewHolder, i: Int) {
        viewHolder.setItem(list[i])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class TeacherViewHolder(
        itemView: View,
        private val mListener: RecyclerViewClickListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var teacherNameTV: TextView = itemView.findViewById(R.id.teacherName)

        fun setItem(item: TeacherModel) {
            teacherNameTV.text = item.teacherName
        }

        override fun onClick(view: View) {
            mListener.onClick(adapterPosition, itemView)
        }
    }

}