package com.furymaxim.schedulenew.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.utils.EmptyViewHolder

class LoadingSection : Section(
    SectionParameters.builder()
        .itemResourceId(R.layout.item_loading)
        .build()
) {
    override fun getContentItemsTotal() = 1
    override fun getItemViewHolder(view: View) = EmptyViewHolder(view)
    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit
}
