package com.furymaxim.schedulenew.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.models.Events
import com.furymaxim.schedulenew.models.YearModel
import com.furymaxim.schedulenew.ui.widgets.CustomCalendarView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RecyclerViewYearAdapter(
    private val years: ArrayList<YearModel>,
    private val context: Context,
    private val inflater: LayoutInflater
) : RecyclerView.Adapter<RecyclerViewYearAdapter.YearViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YearViewHolder {
        return YearViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_item_year,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: YearViewHolder, position: Int) {
        holder.setYear(years[position])
    }

    override fun getItemCount() = years.size

    inner class YearViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val yearTV: TextView = itemView.findViewById(R.id.year)
        private val parent: LinearLayout = itemView.findViewById(R.id.layoutView)

        fun setYear(item: YearModel) {
            yearTV.text = item.year.toString()

            for (i in 0 until 12) {
                generateCalendarMonthLayout(i, item.months.month[i]!!, item.year)
            }
        }

        private fun generateCalendarMonthLayout(index: Int, month: String, year: Int) {
            val view = LayoutInflater.from(context).inflate(R.layout.custom_cal_layout, null)
            val dates = ArrayList<Date>()
            val events = addEventsFromDB()
            val nowCalendar = Calendar.getInstance(Locale.GERMANY)
            val calendar = Calendar.getInstance(Locale.GERMANY)
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, index)
            val monthCalendar = calendar.clone() as Calendar
            monthCalendar.set(Calendar.DAY_OF_MONTH, 1)
            val firstDayOfMonth = monthCalendar[Calendar.DAY_OF_WEEK] - 2
            if (firstDayOfMonth != -1) {
                monthCalendar.add(Calendar.DAY_OF_MONTH, -firstDayOfMonth)
            } else {
                monthCalendar.add(Calendar.DAY_OF_MONTH, firstDayOfMonth - 5)
            }

            while (dates.size < MAX_CALENDAR_DAYS) {
                if (isDateBelongsToCurrentMonth(
                        index,
                        year,
                        monthCalendar.get(Calendar.MONTH),
                        monthCalendar.get(Calendar.YEAR)
                    )
                ) {
                    dates.add(monthCalendar.time)
                } else {
                    val formatter = SimpleDateFormat("dd MMM yyyy", Locale.US)
                    dates.add(formatter.parse("1 Jan 1970")!!)
                }
                monthCalendar.add(Calendar.DAY_OF_MONTH, 1)
            }

            val customCalendarView: CustomCalendarView = view.findViewById(R.id.customCalendarView)
            customCalendarView.setupCalendar(month, dates, calendar, nowCalendar, events, inflater)

            parent.addView(
                customCalendarView,
                LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
            )
        }

        private fun isDateBelongsToCurrentMonth(
            month: Int,
            year: Int,
            monthToCheck: Int,
            yearToCheck: Int
        ): Boolean {
            if (year == yearToCheck) {
                if (month == monthToCheck) {
                    return true
                }
            }
            return false
        }

        private fun addEventsFromDB(): ArrayList<Events> {
            val list = ArrayList<Events>()

            list.add(Events("Тестовое событие", "10:30", "29", "2", "2020"))
            list.add(Events("Тестовое событие", "18:30", "3", "3", "2020"))
            list.add(Events("Тестовое событие", "19:30", "3", "3", "2020"))
            list.add(Events("Тестовое событие", "12:30", "6", "3", "2020"))
            list.add(Events("Тестовое событие", "18:30", "8", "3", "2020"))
            list.add(Events("Тестовое событие", "16:30", "4", "3", "2020"))

            return list
        }
    }

    companion object {

        private const val MAX_CALENDAR_DAYS = 42
    }
}