package com.furymaxim.schedulenew.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.models.Events
import java.text.SimpleDateFormat
import java.util.*

class MyGridAdapter(
    context: Context,
    var dates: List<Date>,
    var currentDateCalendar: Calendar,
    var nowCalendar: Calendar,
    var events: List<Events>
) : ArrayAdapter<Date>(context, R.layout.single_cell_layout) {
    var inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val monthDate = dates[position]
        val dateCalendar = Calendar.getInstance(Locale.GERMANY)
        dateCalendar.time = monthDate

        val displayDay = dateCalendar.get(Calendar.DAY_OF_MONTH)
        val displayMonth = dateCalendar.get(Calendar.MONTH) + 1
        val displayYear = dateCalendar.get(Calendar.YEAR)
        val monthFormat = SimpleDateFormat("MM", Locale.GERMANY)
        val month = monthFormat.format(monthDate)


        val currentMonth = currentDateCalendar.get(Calendar.MONTH) + 1
        val currentYear = currentDateCalendar.get(Calendar.YEAR)
        val currentDay = currentDateCalendar.get(Calendar.DAY_OF_MONTH)

        val nowMonth = nowCalendar.get(Calendar.MONTH) + 1
        val nowYear = nowCalendar.get(Calendar.YEAR)
        val nowDate = nowCalendar.time
        val newMonth = monthFormat.format(nowDate)
        val nowDay = nowCalendar.get(Calendar.DAY_OF_MONTH)

        var view = convertView
        if (view == null) {
            view = inflater.inflate(R.layout.single_cell_layout, parent, false)
        }

        val dayNumber: TextView = view!!.findViewById(R.id.calendarDay)
        val layout: LinearLayout = view.findViewById(R.id.layout)
        val eventIndicator: ImageView = view.findViewById(R.id.eventPointer)

        if (displayYear == 1970) {
            layout.visibility = View.GONE
            // }
        } else {
            dayNumber.text = displayDay.toString()
            if (displayDay == nowDay && month == newMonth && displayYear == nowYear) {
                layout.setBackgroundColor(parent.context.resources.getColor(R.color.colorBlue_600))
            }
            if (doesEventExist(displayDay, month.toInt(), displayYear)) {
                eventIndicator.visibility = View.VISIBLE
                if (displayDay == nowDay && month == newMonth && displayYear == nowYear) {
                    eventIndicator.setImageDrawable(view.context.getDrawable(R.drawable.circle_view_white))
                }
            }
        }
        return view
    }

    override fun getCount(): Int {
        return dates.size
    }

    override fun getPosition(item: Date?): Int {
        return dates.indexOf(item)
    }

    override fun getItem(position: Int): Date? {
        return dates[position]
    }

    private fun doesEventExist(day: Int, month: Int, year: Int): Boolean {
        var value = false
        for (event in events) {
            if (event.day.toInt() == day && event.month.toInt() == month && event.year.toInt() == year) {
                value = true
            }
            if (value) break
        }
        return value
    }
}