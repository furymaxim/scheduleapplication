package com.furymaxim.schedulenew.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.helpers.DateHelper

import com.furymaxim.schedulenew.models.Week
import java.util.*
import kotlin.collections.ArrayList

class WeekAdapter(
    weeks: ArrayList<Week>,
    private val context: Context,
    private val calendar: Calendar,
    private val onItemClick: (Week, Int, Int) -> Unit
) :
    RecyclerView.Adapter<WeekItemViewHolder>() {
    private val weeks = ArrayList<Week>()
    private var daySelected: Int
    private val nowCalendar = calendar.clone() as Calendar
    private val realCalendar = Calendar.getInstance()

    init {
        //  this.calendar.time = Date()
        this.weeks.addAll(weeks)
        this.daySelected = this.calendar.get(Calendar.DAY_OF_WEEK)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekItemViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_week, parent, false)
        return WeekItemViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return weeks.count()
    }

    override fun onBindViewHolder(holder: WeekItemViewHolder, position: Int) {
        holder.configure(
            position,
            calendar,
            nowCalendar,
            realCalendar,
            weeks[position],
            daySelected,
            context,
            onItemClick
        )
    }

    fun selectDay(day: Int) {
        daySelected = day
        notifyDataSetChanged()
    }

    fun getCurrentDay(): Int {
        return daySelected
    }
}

class WeekItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val mondayView: FrameLayout = itemView.findViewById(R.id.monday)
    private val tuesdayView: FrameLayout = itemView.findViewById(R.id.tuesday)
    private val wednesdayView: FrameLayout = itemView.findViewById(R.id.wednesday)
    private val thursdayView: FrameLayout = itemView.findViewById(R.id.thursday)
    private val fridayView: FrameLayout = itemView.findViewById(R.id.friday)
    private val saturdayView: FrameLayout = itemView.findViewById(R.id.saturday)
    private val sundayView: FrameLayout = itemView.findViewById(R.id.sunday)
    private val dateView: TextView = itemView.findViewById(R.id.date)

    fun configure(
        position: Int,
        calendar: Calendar,
        nowCalendar: Calendar,
        realCalendar: Calendar,
        week: Week,
        selected: Int,
        context: Context,
        onDayClick: (Week, Int, Int) -> Unit
    ) {
        calendar.set(Calendar.YEAR, week.year)
        calendar.set(Calendar.WEEK_OF_YEAR, week.number)

        val mondayView = mondayView.getChildAt(0) as TextView
        val tuesdayView = tuesdayView.getChildAt(0) as TextView
        val wednesdayView = wednesdayView.getChildAt(0) as TextView
        val thursdayView = thursdayView.getChildAt(0) as TextView
        val fridayView = fridayView.getChildAt(0) as TextView
        val saturdayView = saturdayView.getChildAt(0) as TextView
        val sundayView = sundayView.getChildAt(0) as TextView

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        mondayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        mondayView.setOnClickListener { onDayClick.invoke(week, Calendar.MONDAY, position) }
        initDayView(
            mondayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.MONDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY)
        tuesdayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        tuesdayView.setOnClickListener { onDayClick.invoke(week, Calendar.TUESDAY, position) }
        initDayView(
            tuesdayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.TUESDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY)
        wednesdayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        wednesdayView.setOnClickListener { onDayClick.invoke(week, Calendar.WEDNESDAY, position) }
        initDayView(
            wednesdayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.WEDNESDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY)
        thursdayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        thursdayView.setOnClickListener { onDayClick.invoke(week, Calendar.THURSDAY, position) }
        initDayView(
            thursdayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.THURSDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY)
        fridayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        fridayView.setOnClickListener { onDayClick.invoke(week, Calendar.FRIDAY, position) }
        initDayView(
            fridayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.FRIDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY)
        saturdayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        saturdayView.setOnClickListener { onDayClick.invoke(week, Calendar.SATURDAY, position) }
        initDayView(
            saturdayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.SATURDAY,
            context
        )

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
        sundayView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        sundayView.setOnClickListener { onDayClick.invoke(week, Calendar.SUNDAY, position) }
        initDayView(
            sundayView,
            calendar,
            nowCalendar,
            realCalendar,
            selected,
            Calendar.SUNDAY,
            context
        )
    }

    @SuppressLint("DefaultLocale")
    private fun initDayView(
        view: TextView,
        calendar: Calendar,
        nowCalendar: Calendar,
        realCalendar: Calendar,
        selected: Int,
        itDay: Int,
        context: Context
    ) {

        if (selected == itDay) {
            dateView.text = DateHelper.formatScheduleTitleDate(calendar.time).capitalize()
            view.setTextColor(ContextCompat.getColor(context, R.color.white))
            initViewBackground(view, calendar, nowCalendar, realCalendar)
        } else {
            if (calendar.get(Calendar.DAY_OF_YEAR) == nowCalendar.get(Calendar.DAY_OF_YEAR)) {
                view.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            } else {
                view.setTextColor(ContextCompat.getColor(context, R.color.text))
            }
            view.setBackgroundResource(0)
        }

        if (calendar.get(Calendar.DAY_OF_YEAR) == realCalendar.get(Calendar.DAY_OF_YEAR)) {
            view.setTextColor(ContextCompat.getColor(context, R.color.colorGreen_900))
            initViewBackground(view, calendar, nowCalendar, realCalendar)
        }
    }

    private fun initViewBackground(
        view: TextView,
        calendar: Calendar,
        nowCalendar: Calendar,
        realCalendar: Calendar
    ) {
        if (calendar.get(Calendar.DAY_OF_YEAR) == nowCalendar.get(Calendar.DAY_OF_YEAR)) {
            view.setBackgroundResource(R.drawable.a_day_now_background)
        } else {
            view.setBackgroundResource(R.drawable.a_day_selected_background)
        }

        if (calendar.get(Calendar.DAY_OF_YEAR) == realCalendar.get(Calendar.DAY_OF_YEAR)) {
            view.setBackgroundResource(R.drawable.a_day_real_background)
        }
    }
}