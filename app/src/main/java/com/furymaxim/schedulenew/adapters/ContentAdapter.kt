package com.furymaxim.schedulenew.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.models.Teacher
import com.furymaxim.schedulenew.ui.activities.ImageViewActivity
import com.squareup.picasso.Picasso

class ContentAdapter(
    private val items: ArrayList<Teacher>,
    private val context: Context,
    private val listener: OnItemClickListener,
    private val activity: AppCompatActivity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Teacher)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_details_schedule, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(items[position], listener)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val image = itemView.findViewById<ImageView>(R.id.image)
        private val tvName = itemView.findViewById<TextView>(R.id.teacherName)
        private val tvPos = itemView.findViewById<TextView>(R.id.teacherPos)

        fun bind(item: Teacher, listener: OnItemClickListener) {
            tvName.text =
                "${item.surname} ${item.name.substring(0, 1)}. ${item.fathername.substring(0, 1)}."
            tvPos.text = item.position
            Picasso.with(context).load(R.drawable.trump).into(image)

            image.setOnClickListener {
                listener.onItemClick(item)

                val i = Intent(context, ImageViewActivity::class.java)
                val transitionName = context.getString(R.string.transition_string)
                val transitionActivityOptions =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity,
                        image,
                        transitionName
                    )

                context.startActivity(i, transitionActivityOptions.toBundle())
            }
        }
    }

}
