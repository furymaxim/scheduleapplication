package com.furymaxim.schedulenew.api.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class NewsResponse(val items: ArrayList<News>)

@Parcelize
data class News(
    val id: String,
    val text: String,
    val title: String,
    val date: String,
    val images: ArrayList<String>
) :
    Parcelable