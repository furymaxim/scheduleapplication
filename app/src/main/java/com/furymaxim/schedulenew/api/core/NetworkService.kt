package com.furymaxim.schedulenew.api.core

import com.furymaxim.schedulenew.BuildConfig
import com.furymaxim.schedulenew.api.dto.LessonResponse
import com.furymaxim.schedulenew.models.Lesson
import com.furymaxim.schedulenew.models.Teacher
import com.furymaxim.schedulenew.models.MyDate
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkService {

    fun get(): APIInterface {
        val builder = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(AcceptJsonInterceptor())
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }

        val client = builder.build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BaseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

        return retrofit.create(APIInterface::class.java)
    }

    companion object {
        const val BaseUrl = "http://167.172.99.157:3000/"
        fun getInstance(): APIInterface {
            return NetworkService().get()
        }

        // здесь мы отправляем запрос...
        fun getSchedule(start: MyDate, end: MyDate): LessonResponse {
            val items = ArrayList<Lesson>()

            if (start.month.toInt() == 2) {
                items.add(
                    Lesson(
                        "11",
                        "2020-02-21T11:50:00.000Z",
                        "2020-02-21T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-21T13:50:00.000Z",
                        "2020-02-21T15:10:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-21T15:30:00.000Z",
                        "2020-02-21T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-22T00:00:00.000Z",
                        "2020-02-22T02:00:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 215",
                        "Упр.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-22T13:50:00.000Z",
                        "2020-02-22T15:10:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-23T08:30:00.000Z",
                        "2020-02-23T10:00:00.000Z",
                        "Теория информации",
                        getTeachers(),
                        "Аудитория 613",
                        "Лаб.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-23T10:10:00.000Z",
                        "2020-02-23T11:40:00.000Z",
                        "Основы электроники",
                        getTeachers(),
                        "Аудитория 71",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-23T11:50:00.000Z",
                        "2020-02-23T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 215",
                        "Упр.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-23T13:50:00.000Z",
                        "2020-02-23T15:10:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-24T11:50:00.000Z",
                        "2020-02-24T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 215",
                        "Упр.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-24T13:50:00.000Z",
                        "2020-02-24T15:10:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-24T15:30:00.000Z",
                        "2020-02-24T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-25T08:30:00.000Z",
                        "2020-02-25T10:00:00.000Z",
                        "Теория информации",
                        getTeachers(),
                        "Аудитория 613",
                        "Лаб.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-25T10:10:00.000Z",
                        "2020-02-25T11:40:00.000Z",
                        "Основы электроники",
                        getTeachers(),
                        "Аудитория 71",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-26T11:50:00.000Z",
                        "2020-02-26T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 215",
                        "Упр.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-26T13:50:00.000Z",
                        "2020-02-26T15:10:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-26T15:30:00.000Z",
                        "2020-02-26T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-27T11:50:00.000Z",
                        "2020-02-27T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 215",
                        "Упр.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-28T10:10:00.000Z",
                        "2020-02-28T11:40:00.000Z",
                        "Основы электроники",
                        getTeachers(),
                        "Аудитория 71",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-29T15:30:00.000Z",
                        "2020-02-29T16:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-02-29T18:30:00.000Z",
                        "2020-02-29T20:00:00.000Z",
                        "Теория информации",
                        getTeachers(),
                        "Аудитория 613",
                        "Лаб.",
                        "Lorem Impust Ryuba Text"
                    )
                )
            }

            if (start.month.toInt() == 3) {
                items.add(
                    Lesson(
                        "11",
                        "2020-03-01T15:30:00.000Z",
                        "2020-03-01T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-03-02T12:30:00.000Z",
                        "2020-03-02T16:00:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 313",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-03-30T15:30:00.000Z",
                        "2020-03-01T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-03-31T12:30:00.000Z",
                        "2020-03-02T16:00:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 313",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-03-04T11:50:00.000Z",
                        "2020-03-04T13:20:00.000Z",
                        "Программирование на Java",
                        getTeachers(),
                        "Аудитория 314",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
            }

            if (start.month.toInt() == 4) {
                items.add(
                    Lesson(
                        "11",
                        "2020-04-01T11:30:00.000Z",
                        "2020-04-01T16:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
                items.add(
                    Lesson(
                        "11",
                        "2020-04-01T16:30:00.000Z",
                        "2020-04-02T18:00:00.000Z",
                        "Информационные технологии",
                        getTeachers(),
                        "Аудитория 313",
                        "Упражнения",
                        "Lorem Impust Ryuba Text"
                    )
                )
            }

            if (start.month.toInt() == 10) {
                items.add(
                    Lesson(
                        "11",
                        "2020-10-01T15:30:00.000Z",
                        "2020-10-01T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 71",
                        "Лек.",
                        "Lorem Impust Ryuba Text"
                    )
                )
            }

            if (start.month.toInt() == 11) {
                items.add(
                    Lesson(
                        "11",
                        "2020-11-01T15:30:00.000Z",
                        "2020-11-01T17:00:00.000Z",
                        "ООП",
                        getTeachers(),
                        "Аудитория 918",
                        "М",
                        "Lorem Impust Ryuba Text"
                    )
                )
            }

            return LessonResponse(items)
        }

        private fun getTeachers(): ArrayList<Teacher> {
            val list = ArrayList<Teacher>()
            list.add(Teacher("01", "Степнов", "Роман", "Дмитриевич", "Доцент"))
            list.add(Teacher("02", "Фролов", "Роман", "Дмитриевич", "Професcор"))

            return list
        }
    }
}