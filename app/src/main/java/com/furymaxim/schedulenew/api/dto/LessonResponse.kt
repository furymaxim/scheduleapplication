package com.furymaxim.schedulenew.api.dto

import com.furymaxim.schedulenew.models.Lesson

data class LessonResponse(val items: ArrayList<Lesson>)
