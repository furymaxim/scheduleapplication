package com.furymaxim.schedulenew.api.core

import com.furymaxim.schedulenew.api.dto.NewsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface APIInterface {

    @GET("news")
    fun getNews(@Query("page") pageIndex: Int): Single<NewsResponse>

}