package com.furymaxim.schedulenew.ui.activities

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.ui.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        bottomNavigation.setOnNavigationItemSelectedListener(navigationListener)
        bottomNavigation.selectedItemId = R.id.nav_schedule

        if (savedInstanceState == null) {
            if (isBundleEmpty()) {
                switchFragment(ScheduleFragment.newInstance())
            } else {
                supportFragmentManager.beginTransaction()
                    .replace(
                        R.id.container,
                        ScheduleWeekViewFragment.newInstance(
                            getDayFromExtras(),
                            getMonthFromExtras(),
                            getYearFromExtras()
                        ),
                        "fragmentSchedule"
                    )
                    .commit()
            }
        }
    }

    private val navigationListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_schedule -> {
                    switchFragment(ScheduleFragment.newInstance())
                }
                R.id.nav_profile -> {
                    switchFragment(ProfileFragment.newInstance())
                }
                R.id.nav_news -> {
                    switchFragment(NewsFragment.newInstance())
                }
                R.id.nav_teachers -> {
                    switchFragment(TeachersFragment.newInstance())
                }
            }
            true
        }

    private fun isBundleEmpty(): Boolean {
        if (intent.getStringExtra("day") != null) {
            return false
        }
        return true
    }

    private fun getDayFromExtras() = intent.getStringExtra("day")
    private fun getMonthFromExtras() = intent.getStringExtra("month")
    private fun getYearFromExtras() = intent.getStringExtra("year")

    private fun switchFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                fragment
            )
            .commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }
}
