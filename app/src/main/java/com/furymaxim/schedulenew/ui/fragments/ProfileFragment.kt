package com.furymaxim.schedulenew.ui.fragments

import androidx.fragment.app.Fragment

import com.furymaxim.schedulenew.R

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    companion object {
        fun newInstance() = ProfileFragment()
    }

}
