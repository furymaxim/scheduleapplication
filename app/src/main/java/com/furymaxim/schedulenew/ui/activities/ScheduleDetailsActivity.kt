package com.furymaxim.schedulenew.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.ContentAdapter
import com.furymaxim.schedulenew.models.Teacher
import kotlinx.android.synthetic.main.activity_schedule_details.*

class ScheduleDetailsActivity : AppCompatActivity() {

    var flag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_details)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)

        lessonTitle.text = getTitleFromExtras()
        //lessonDesc.text = getDescriptionFromExtras()
        lessonDuration.text = getDurationFromExtras() + " мин."
        lessonStart.text = getStartTimeFromExtras()
        lessonEnd.text = getEndTimeFromExtras()

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ContentAdapter(getTeacherFromExtras(), this, object :
            ContentAdapter.OnItemClickListener {
            override fun onItemClick(item: Teacher) {

            }
        }, this)

        button.setOnClickListener {
            if (!flag) {
                button.setBackgroundResource(R.drawable.round_btn_green)
                button.text = "Я ИДУ!"
            } else {
                button.setBackgroundResource(R.drawable.round_btn)
                button.text = "Я ПОЙДУ"
            }
            flag = !flag
        }

        back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("day", getDayFromExtras())
            intent.putExtra("month", getMonthFromExtras())
            intent.putExtra("year", getYearFromExtras())

            startActivity(intent)
        }
    }

    private fun getTitleFromExtras() = intent.getStringExtra("title")
    private fun getDescriptionFromExtras() = intent.getStringExtra("description")
    private fun getStartTimeFromExtras() = intent.getStringExtra("start")
    private fun getEndTimeFromExtras() = intent.getStringExtra("end")
    private fun getDurationFromExtras() = intent.getStringExtra("duration")
    private fun getPlaceFromExtras() = intent.getStringExtra("place")
    private fun getTeacherFromExtras() = intent.getParcelableArrayListExtra<Teacher>("teachers")
    private fun getTypeFromExtras() = intent.getStringExtra("type")
    private fun getDayFromExtras() = intent.getStringExtra("day")
    private fun getMonthFromExtras() = intent.getStringExtra("month")
    private fun getYearFromExtras() = intent.getStringExtra("year")
}
