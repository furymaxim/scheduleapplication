package com.furymaxim.schedulenew.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.ui.fragments.tabs.TypeTrainingFragmentTab
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_schedule.*

class ScheduleFragment : Fragment(R.layout.fragment_schedule) {

    var mTitles = arrayOf("Индивидуальные", "Групповые")
    private var lastSelect = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTabLayout()

        pager!!.adapter = PagerAdapter(childFragmentManager)
        pager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                update(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        pager!!.currentItem = lastSelect

    }

    private fun update(position: Int) {
        for (i in mTitles.indices) {

            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected = false
            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).setTextColor(
                ContextCompat.getColor(context!!, R.color.colorPrimary)
            )
            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text)
                .setBackgroundResource(
                    R.drawable.selector_tabs
                )

            if (i == position) {
                tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected =
                    true
                tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.white
                    )
                )
            }
        }
        lastSelect = position
    }

    private fun setupTabLayout() {
        for (current in mTitles.indices) {
            val tab: TabLayout.Tab = tabs!!.newTab()

            tab.setCustomView(R.layout.tabs)

            (tab.customView as FrameLayout).findViewById<TextView>(R.id.text).text =
                mTitles[current]

            tab.customView!!.setOnClickListener { pager!!.currentItem = current }

            tabs!!.addTab(tab)
        }
    }

    override fun onResume() {
        super.onResume()
        update(pager!!.currentItem)
    }

    inner class PagerAdapter(fm: FragmentManager?) :
        FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getPageTitle(position: Int): CharSequence? {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                TypeTrainingFragmentTab.newInstance(true)
            } else {
                TypeTrainingFragmentTab.newInstance(false)
            }
        }

        override fun getCount(): Int {
            return mTitles.size
        }
    }

    companion object {
        fun newInstance() =
            ScheduleFragment()
    }


}
