package com.furymaxim.schedulenew.ui.fragments.tabs


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.RecyclerViewYearAdapter
import com.furymaxim.schedulenew.helpers.MeasureHelper
import com.furymaxim.schedulenew.models.MonthModel
import com.furymaxim.schedulenew.models.YearModel
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_type_training_tab.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class TypeTrainingFragmentTab : Fragment(R.layout.fragment_type_training_tab) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.ProgressDialogLoad)
            .build()

        dialog.show()

        Handler(Looper.getMainLooper()).postDelayed({
            setupRecyclerView()
            dialog.dismiss()
        }, 2000)
    }


    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.adapter = RecyclerViewYearAdapter(getYears(), context!!, layoutInflater)

        recyclerView.smoothScrollBy(0, MeasureHelper.getScrollByYPos(context!!))
    }

    @SuppressLint("UseSparseArrays")
    private fun getYears(): ArrayList<YearModel> {
        val list = ArrayList<YearModel>()
        val monthArr = resources.getStringArray(R.array.months)
        val map1 = HashMap<Int, String>()

        for (i in 0 until 12) {
            map1[i] = monthArr[i]
        }

        val mapMonth = MonthModel(map1)

        list.add(YearModel(getYear(-1), mapMonth))
        list.add(YearModel(getYear(0), mapMonth))
        list.add(YearModel(getYear(1), mapMonth))

        return list
    }

    private fun getYear(flag: Int): Int {
        val calendar = Calendar.getInstance()
        var year = 0
        when (flag) {
            -1 -> {
                calendar.add(Calendar.YEAR, -1)
                year = calendar.get(Calendar.YEAR)
            }
            0 -> year = calendar.get(Calendar.YEAR)
            1 -> {
                calendar.add(Calendar.YEAR, 1)
                year = calendar.get(Calendar.YEAR)
            }
        }

        return year
    }

    companion object {
        fun newInstance(flag: Boolean): TypeTrainingFragmentTab {
            val myFragment = TypeTrainingFragmentTab()
            val args = Bundle()
            args.putBoolean("flag", flag)
            myFragment.arguments = args

            return myFragment
        }


    }


}
