package com.furymaxim.schedulenew.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.WeekAdapter
import com.furymaxim.schedulenew.api.core.NetworkService
import com.furymaxim.schedulenew.api.dto.LessonResponse
import com.furymaxim.schedulenew.helpers.DateHelper
import com.furymaxim.schedulenew.helpers.StringHelper
import com.furymaxim.schedulenew.models.Lesson
import com.furymaxim.schedulenew.models.MyDate
import com.furymaxim.schedulenew.models.Week
import com.furymaxim.schedulenew.ui.activities.MainActivity
import com.furymaxim.schedulenew.ui.activities.ScheduleDetailsActivity
import com.furymaxim.schedulenew.ui.widgets.EventView
import com.yarolegovich.discretescrollview.DSVOrientation
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_schedule_week_view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ScheduleWeekViewFragment : Fragment(R.layout.fragment_schedule_week_view) {

    lateinit var itemsAll: ArrayList<Lesson>
    private var activeDay = -1
    private var calendar = Calendar.getInstance()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val addedItemsForMonths = ArrayList<Int>()
        val day = arguments!!.getString("day")
        val month = arguments!!.getString("month")
        val year = arguments!!.getString("year")

        val formatter = SimpleDateFormat("dd MM yyyy", Locale.US)
        val date = formatter.parse("$day $month $year")!!

        //calendar.time = date
        calendar.set(Calendar.DAY_OF_MONTH, day!!.toInt())
        calendar.set(Calendar.MONTH, month!!.toInt() - 1)
        calendar.set(Calendar.YEAR, year!!.toInt())
        calendar.get(Calendar.DAY_OF_WEEK)

        val currentWeekNumber = calendar.get(Calendar.WEEK_OF_YEAR)
        val currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        activeDay = currentDayOfWeek

        val dialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.ProgressDialog)
            .build()

        dialog.show()

        itemsAll = loadSchedule(
            DateHelper.getStartOfMonth(calendar),
            DateHelper.getEndOfMonth(calendar)
        ).items

        Handler(Looper.getMainLooper()).postDelayed({
            dialog.dismiss()
        }, 2000)

        initWeeksLine()
        initSchedule(
            Week(calendar.get(Calendar.WEEK_OF_YEAR), calendar.get(Calendar.YEAR)),
            currentDayOfWeek
        )

        weekView.addOnItemChangedListener { _, adapterPosition ->
            val listOfDays = ArrayList<Int>()
            val nextCalendar = calendar.clone() as Calendar
            val prevCalendar = calendar.clone() as Calendar

            var haveToLoad = false
            var myMonthNext = 0
            var myMonthPrev = 0
            var i = 0

            nextCalendar.set(Calendar.WEEK_OF_YEAR, adapterPosition + 1)
            for (x in 1..7) {
                nextCalendar.set(Calendar.DAY_OF_WEEK, x)
                listOfDays.add(nextCalendar.get(Calendar.DAY_OF_MONTH))
            }

            for (myDay in listOfDays) {
                if (myDay == 1) {
                    haveToLoad = true
                    myMonthNext = if (i != 0) {
                        nextCalendar.get(Calendar.MONTH) + 1
                    } else {
                        nextCalendar.get(Calendar.MONTH) + 2
                    }
                    myMonthPrev = if (i != 0) {
                        prevCalendar.get(Calendar.MONTH)
                    } else {
                        prevCalendar.get(Calendar.MONTH)
                    }
                    break
                }
                i++
            }

            if (haveToLoad) {
                nextCalendar.add(Calendar.MONTH, 1)
                nextCalendar.set(
                    Calendar.DATE,
                    nextCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
                )
                prevCalendar.add(Calendar.MONTH, -1)
                prevCalendar.set(
                    Calendar.DATE,
                    prevCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
                )
                nextCalendar.time = nextCalendar.time
                prevCalendar.time = prevCalendar.time

                val myDay = nextCalendar.get(Calendar.DAY_OF_MONTH)
                val myYear = nextCalendar.get(Calendar.YEAR)

                val myPrevDay = prevCalendar.get(Calendar.DAY_OF_MONTH)
                val myPrevYear = prevCalendar.get(Calendar.YEAR)

                var load = false

                if (!addedItemsForMonths.contains(myMonthNext)) {
                    load = true
                    dialog.show()
                    val newItems =
                        loadSchedule(
                            MyDate("1", myMonthNext.toString(), myYear.toString()),
                            MyDate(myDay.toString(), myMonthNext.toString(), myYear.toString())
                        ).items
                    itemsAll.addAll(newItems)
                    addedItemsForMonths.add(myMonthNext)
                }

                if (!addedItemsForMonths.contains(myMonthPrev)) {
                    if (!load) {
                        dialog.show()
                        load = true
                    }

                    val prevItems =
                        loadSchedule(
                            MyDate("1", myMonthPrev.toString(), myPrevYear.toString()),
                            MyDate(
                                myPrevDay.toString(),
                                myMonthPrev.toString(),
                                myPrevYear.toString()
                            )
                        ).items
                    itemsAll.addAll(prevItems)
                    addedItemsForMonths.add(myMonthPrev)
                }

                if (load) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        dialog.dismiss()
                    }, 2000)
                }
            }

            initSchedule(
                Week(adapterPosition + 1, year.toInt()),
                activeDay
            )
        }

        weekView.scrollToPosition(currentWeekNumber - 1)

        back.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.container, ScheduleFragment.newInstance())
                .commit()
        }
    }

    private fun initWeeksLine() {
        weekView.adapter =
            WeekAdapter(
                generateWeeks(calendar),
                context!!,
                calendar
            ) { week: Week, weekDay: Int, _: Int ->
                val adapter = weekView.adapter as WeekAdapter

                activeDay = weekDay

                if (adapter.getCurrentDay() != weekDay) {
                    adapter.selectDay(activeDay)
                } else {
                    moveToFirstEvent(week, weekDay)
                }
            }

        weekView.setSlideOnFlingThreshold(4000)
        weekView.setItemTransitionTimeMillis(150)
        weekView.setOrientation(DSVOrientation.HORIZONTAL)
        weekView.setOffscreenItems(0)
        weekView.setOverScrollEnabled(false)
    }

    private fun initSchedule(week: Week, weekday: Int) {
        val items = getItemsForDayOfWeek(week, weekday)
        dayLineContainerView.removeAllViews()
        if (items.isEmpty()) return

        for (item in items) {
            val view = EventView.init(item, context)
            view.setOnClickListener {
                val intent = Intent(activity as MainActivity, ScheduleDetailsActivity::class.java)
                val duration = getDuration(item.start, item.end)
                val startTime = StringHelper.getTime(item.start)
                val endTime = StringHelper.getTime(item.end)
                val day = StringHelper.getDay(item.start)
                val month = StringHelper.getMonth(item.start)
                val year = StringHelper.getYear(item.start)

                intent.putExtra("teachers", item.teachers)
                intent.putExtra("duration", duration)
                intent.putExtra("place", item.place)
                intent.putExtra("title", item.title)
                intent.putExtra("start", startTime)
                intent.putExtra("end", endTime)
                intent.putExtra("type", item.type)
                intent.putExtra("day", day)
                intent.putExtra("month", month)
                intent.putExtra("year", year)

                startActivity(intent)
            }
            dayLineContainerView.addView(view)
        }
        Handler().postDelayed({
            eventContainerView.smoothScrollTo(
                0,
                EventView.getOffsetForLesson(items[0], context)
            )
        }, 650)
    }

    private fun getDuration(start: String, end: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)

        format.timeZone = TimeZone.getTimeZone("UTC")

        val startDate = format.parse(start)
        val endDate = format.parse(end)

        return DateHelper.getDateDiff(startDate, endDate, TimeUnit.MINUTES).toString()
    }

    private fun loadSchedule(start: MyDate, end: MyDate): LessonResponse {
        return NetworkService.getSchedule(start, end)
    }

    private fun moveToFirstEvent(week: Week, weekday: Int) {
        val items = getItemsForDayOfWeek(week, weekday)

        if (items.isEmpty()) return
        eventContainerView.smoothScrollTo(0, EventView.getOffsetForLesson(items[0], context))
    }

    private fun getItemsForDayOfWeek(week: Week, weekday: Int): ArrayList<Lesson> {
        var itemsForDay = ArrayList<Lesson>()
        val calendar = calendar.clone() as Calendar
        val numberOfWeek = week.number
        when (weekday) {
            1 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 1
            }
            2 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 2
            }
            3 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 3
            }
            4 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 4
            }
            5 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 5
            }
            6 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 6
            }
            7 -> {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY)
                calendar.set(Calendar.WEEK_OF_YEAR, numberOfWeek)
                itemsForDay =
                    loadItems(
                        calendar.get(Calendar.YEAR).toString(),
                        (calendar.get(Calendar.MONTH) + 1).toString(),
                        calendar.get(Calendar.DAY_OF_MONTH).toString()
                    )
                activeDay = 7
            }
        }
        return itemsForDay
    }

    private fun loadItems(year: String, month: String, dayOfMonth: String): ArrayList<Lesson> {
        val list = ArrayList<Lesson>()

        for (item in itemsAll) {
            val pattern = if (month.length == 1) {
                if (dayOfMonth.length == 1) {
                    "${year}-0${month}-0${dayOfMonth}"
                } else {
                    "${year}-0${month}-${dayOfMonth}"
                }

            } else {
                if (dayOfMonth.length == 1) {
                    "${year}-${month}-0${dayOfMonth}"
                } else {
                    "${year}-${month}-${dayOfMonth}"
                }
            }
            if (item.start.contains(pattern)) {
                list.add(item)
            }
        }
        return list
    }

    companion object {
        fun newInstance(day: String, month: String, year: String): ScheduleWeekViewFragment {
            val myFragment = ScheduleWeekViewFragment()
            val args = Bundle()
            args.putString("day", day)
            args.putString("month", month)
            args.putString("year", year)
            myFragment.arguments = args

            return myFragment
        }

        fun generateWeeks(calendar: Calendar): ArrayList<Week> {
            val items = ArrayList<Week>()

            for (x in 1..52) {
                items.add(Week(x, calendar.get(Calendar.YEAR)))
            }

            return items
        }
    }


}
