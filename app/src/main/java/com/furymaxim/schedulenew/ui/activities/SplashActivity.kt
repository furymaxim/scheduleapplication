package com.furymaxim.schedulenew.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.helpers.TempStorage
import com.furymaxim.schedulenew.utils.Params
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        imageView.animate().alpha(1.0f).duration = 500

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val preferences = TempStorage.getPreference(this)

            if (preferences.getBoolean(Params.LOGIN, false)) {
                showMainScreen()
            } else {
                showLogin()
            }
        }, 800)

    }

    private fun showMainScreen() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showLogin() {
        val offset = resources.getDimension(R.dimen.inputBlockHeight)
        imageView.animate().translationYBy(-offset).setStartDelay(0).duration = 800
        val intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
        finish()
    }

}
