package com.furymaxim.schedulenew.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.furymaxim.schedulenew.R
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_auth.passwordInput
import kotlinx.android.synthetic.main.activity_auth.phoneInput
import kotlinx.android.synthetic.main.activity_auth.reg
import kotlinx.android.synthetic.main.activity_registration.*


class RegistrationActivity : AppCompatActivity() {

    var isMaskFilled = false
    var userPhone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)

        setPhoneMask()

        reg.setOnClickListener {
            val inputManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                this.currentFocus!!.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            if (isMaskFilled && passwordInput.text!!.isNotBlank() && passwordInput.text.toString().length > 3) {
                if (passwordInput.text.toString() == passwordRepeatInput.text.toString()) {
                    // do reg
                    val dialog = SpotsDialog.Builder()
                        .setContext(this)
                        .setCancelable(false)
                        .setTheme(R.style.ProgressDialogReg)
                        .build()

                    dialog.show()

                    Handler(Looper.getMainLooper()).postDelayed({
                        dialog.dismiss()
                        Toast.makeText(this, "Вы успешно зарегистрированы!", Toast.LENGTH_SHORT)
                            .show()
                        val intent = Intent(this, AuthActivity::class.java)
                        startActivity(intent)
                        finish()
                    }, 2000)
                } else {
                    Toast.makeText(this, "Пароли не совпадают", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun setPhoneMask() {
        val affineFormats: MutableList<String> = ArrayList()
        affineFormats.add("8 ([000]) [000]-[00]-[00]")

        val listener =
            MaskedTextChangedListener.installOn(
                phoneInput,
                "+7 ([000]) [000]-[00]-[00]",
                affineFormats, AffinityCalculationStrategy.PREFIX,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, @NonNull extractedValue: String, @NonNull formattedValue: String) {
                        isMaskFilled = maskFilled
                        userPhone = if (isMaskFilled) {
                            formattedValue
                        } else {
                            ""
                        }

                    }
                }
            )

        phoneInput.hint = listener.placeholder()
    }
}
