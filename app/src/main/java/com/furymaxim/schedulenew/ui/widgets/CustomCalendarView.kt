package com.furymaxim.schedulenew.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.MyGridAdapter
import com.furymaxim.schedulenew.models.Events
import com.furymaxim.schedulenew.ui.fragments.ScheduleWeekViewFragment
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CustomCalendarView : LinearLayout {

    var dates = ArrayList<Date>()
    var events = ArrayList<Events>()
    var year: String? = null
    lateinit var mContext: Context
    var gridView: GridView? = null
    var monthView: TextView? = null
    var inflater: LayoutInflater? = null
    private val monthFormat = SimpleDateFormat("MM", Locale("ru"))
    private val yearFormat = SimpleDateFormat("yyyy", Locale("ru"))
    private val dayFormat = SimpleDateFormat("dd", Locale("ru"))

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs) {
        mContext = ctx
        initLayout()

        gridView!!.setOnItemClickListener { _, v, i, _ ->
            val day = dayFormat.format(dates[i])
            val month = monthFormat.format(dates[i])
            val year = yearFormat.format(dates[i])
            val myEvents = ArrayList<Events>()
            val stringBuilder = StringBuilder("")


            if (year.toInt() != 1970) {

                for (event in events) {
                    if (event.day.toInt() == day.toInt() && event.month.toInt() == month.toInt() && event.year.toInt() == year.toInt()) {
                        myEvents.add(event)
                    }
                }

                for (currentDayEvent in myEvents) {
                    stringBuilder.append("Title: ${currentDayEvent.event} Time: ${currentDayEvent.time}\n")
                }

                (v.context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ScheduleWeekViewFragment.newInstance(day, month, year))
                    .commit()

                //Toast.makeText(context, "$day $month $year \n $stringBuilder", Toast.LENGTH_SHORT).show()
            }

        }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )


    private fun initLayout() {
        val view = View.inflate(mContext, R.layout.calendar_layout, this)
        gridView = view.findViewById(R.id.gridView)
        monthView = view.findViewById(R.id.monthView)
    }

    fun setupCalendar(
        month: String,
        dates: ArrayList<Date>,
        calendarCurrentMonthAdapter: Calendar,
        nowCalendar: Calendar,
        events: ArrayList<Events>,
        inflater: LayoutInflater
    ) {
        this.dates.clear()
        monthView!!.text = month
        this.dates = dates
        this.events = events
        this.inflater = inflater
        gridView!!.adapter =
            MyGridAdapter(context, this.dates, calendarCurrentMonthAdapter, nowCalendar, events)
    }


}