package com.furymaxim.victoriakotlin.ui.activities

import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.viewpager.widget.ViewPager
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.SliderAdapter
import kotlinx.android.synthetic.main.activity_news_details.*
import java.util.*

@Suppress("DEPRECATION")
class NewsDetailsActivity : AppCompatActivity() {
    private var currentPage = 0
    private var NUM_PAGES = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)

        init()

/*      ViewCompat.setTransitionName(newsTitle, VIEW_NAME_TITLE)
        ViewCompat.setTransitionName(newsDate, VIEW_NAME_DATE)
        ViewCompat.setTransitionName(newsText, VIEW_NAME_TEXT)*/
        ViewCompat.setTransitionName(layout, VIEW_NAME_LAYOUT)
        ViewCompat.setTransitionName(imageLayout, VIEW_NAME_IMAGE)

        newsDate.text = getDateFromExtras()
        newsText.text = getTextFromExtras()
        newsTitle.text = getTitleFromExtras()
    }

    private fun getTextFromExtras() = intent.getStringExtra("text")
    private fun getDateFromExtras() = intent.getStringExtra("date")
    private fun getImageLinksFromExtras() = intent.getStringArrayListExtra("imageLinks")
    private fun getTitleFromExtras() = intent.getStringExtra("title")

    private fun init() {
        pager.adapter = SliderAdapter(applicationContext, getImageLinksFromExtras())
        indicator!!.setViewPager(pager)
        NUM_PAGES = if (getImageLinksFromExtras()!!.size == 0) {
            1
        } else {
            getImageLinksFromExtras()!!.size
        }
        val handler = Handler()
        val update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            pager.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        indicator!!.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                currentPage = position
            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })
    }

    companion object {
        const val VIEW_NAME_TITLE = "detail:title"
        const val VIEW_NAME_DATE = "detail:date"
        const val VIEW_NAME_TEXT = "detail:text"
        const val VIEW_NAME_LAYOUT = "detail:layout"
        const val VIEW_NAME_IMAGE = "detail:image"
    }

}
