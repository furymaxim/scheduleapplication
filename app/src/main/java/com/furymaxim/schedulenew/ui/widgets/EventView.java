package com.furymaxim.schedulenew.ui.widgets;

import android.app.ActionBar;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.furymaxim.schedulenew.R;
import com.furymaxim.schedulenew.models.Lesson;

import java.util.Random;

public class EventView {

    public static View init(Lesson lesson, Context context) {
        View view = View.inflate(context, R.layout.item_event, null);
        TextView descriptionView = view.findViewById(R.id.description);

        int oneHourItem = (context.getResources().getDimensionPixelSize(R.dimen.event_margin) + context.getResources().getDimensionPixelSize(R.dimen.event_line_height));
        float pixelPerMinute = oneHourItem / 60f;
        int height = (int) (lesson.getDurationInMinutes() * pixelPerMinute);
        //if(height < 170) height = 170;
        int topOffset = (int) (lesson.getStartMinutes() * pixelPerMinute);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, height);
        params.topMargin = topOffset + oneHourItem;

        view.setLayoutParams(params);

        Random random = new Random();
        int color = random.nextInt((5) + 1);
        if (color == 0) {
            descriptionView.setBackgroundResource(R.drawable.a_event_blue);
        } else if (color == 1) {
            descriptionView.setBackgroundResource(R.drawable.a_event_green);
        } else if (color == 2) {
            descriptionView.setBackgroundResource(R.drawable.a_event_orange);
        } else if (color == 3) {
            descriptionView.setBackgroundResource(R.drawable.a_event_purple);
        } else if (color == 4) {
            descriptionView.setBackgroundResource(R.drawable.a_event_red);
        } else if (color == 5) {
            descriptionView.setBackgroundResource(R.drawable.a_event_yellow);
        }

        descriptionView.setText(lesson.getDescription());

        return view;
    }

    public static int getOffsetForLesson(Lesson lesson, Context context) {
        int oneHourItem = (context.getResources().getDimensionPixelSize(R.dimen.event_margin) + context.getResources().getDimensionPixelSize(R.dimen.event_line_height));
        float pixelPerMinute = oneHourItem / 60f;
        int topOffset = (int) (lesson.getStartMinutes() * pixelPerMinute);
        return topOffset + oneHourItem;
    }


}
