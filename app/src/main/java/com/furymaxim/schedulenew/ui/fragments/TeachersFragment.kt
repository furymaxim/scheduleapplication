package com.furymaxim.schedulenew.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.TeachersRecyclerViewAdapter
import com.furymaxim.schedulenew.models.TeacherModel
import com.furymaxim.schedulenew.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.fragment_teachers.*

class TeachersFragment : Fragment(R.layout.fragment_teachers), RecyclerViewClickListener {

    lateinit var teachersList: MutableList<TeacherModel>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = TeachersRecyclerViewAdapter(getTeachers(), this)
    }

    private fun getTeachers(): MutableList<TeacherModel> {
        teachersList = ArrayList()
        teachersList.add(TeacherModel("Акиншина Кристина Валерьевна", "Программист"))
        teachersList.add(TeacherModel("Агапов Михаил Петрович", "Андроид - разработчик"))

        return teachersList
    }

    override fun onClick(position: Int, view: View) {
        Toast.makeText(context, teachersList[position].info, Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance() = TeachersFragment()
    }

}
