package com.furymaxim.schedulenew.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.helpers.TempStorage
import com.furymaxim.schedulenew.utils.Params
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    var isMaskFilled = false
    var userPhone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)

        setPhoneMask()

        auth.setOnClickListener {
            if (this.currentFocus != null) {
                val inputManager: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    this.currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
            if (isMaskFilled && passwordInput.text!!.isNotBlank() && passwordInput.text.toString().length > 3) {
                if (isPhoneValid(userPhone)) {
                    val preferences = TempStorage.getEditor(this)
                    preferences.putBoolean(Params.LOGIN, true)
                        .commit()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this, "Проверьте введенные данные", Toast.LENGTH_SHORT).show()
                }
            }
        }

        reg.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setPhoneMask() {
        val affineFormats: MutableList<String> = ArrayList()
        affineFormats.add("8 ([000]) [000]-[00]-[00]")

        val listener =
            MaskedTextChangedListener.installOn(
                phoneInput,
                "+7 ([000]) [000]-[00]-[00]",
                affineFormats, AffinityCalculationStrategy.PREFIX,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, @NonNull extractedValue: String, @NonNull formattedValue: String) {
                        isMaskFilled = maskFilled
                        userPhone = if (isMaskFilled) {
                            formattedValue
                        } else {
                            ""
                        }

                    }
                }
            )

        phoneInput.hint = listener.placeholder()
    }

    private fun isPhoneValid(phone: String): Boolean {
        //check on server

        return phone.isNotBlank()
    }
}
