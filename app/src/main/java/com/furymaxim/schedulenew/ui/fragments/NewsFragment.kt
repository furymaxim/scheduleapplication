package com.furymaxim.schedulenew.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.children
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.schedulenew.R
import com.furymaxim.schedulenew.adapters.LoadingSection
import com.furymaxim.schedulenew.adapters.NewsItemSection
import com.furymaxim.schedulenew.api.core.NetworkService
import com.furymaxim.schedulenew.api.dto.News
import com.furymaxim.schedulenew.helpers.DateHelper
import com.furymaxim.schedulenew.helpers.dispatchUpdatesTo
import com.furymaxim.schedulenew.ui.activities.MainActivity
import com.furymaxim.schedulenew.viewmodel.NewsListViewModel
import com.furymaxim.victoriakotlin.ui.activities.NewsDetailsActivity
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.item_news.view.*

class NewsFragment : BaseFragment(R.layout.fragment_news) {
    lateinit var viewModel: NewsListViewModel

    private val itemsSection = NewsItemSection(emptyList()) { news, v ->
        val intent = Intent(activity as MainActivity, NewsDetailsActivity::class.java)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity as MainActivity,
            //androidx.core.util.Pair(v.titleView, NewsDetailsActivity.VIEW_NAME_TITLE),
            Pair(v.textView, NewsDetailsActivity.VIEW_NAME_TEXT),
            //androidx.core.util.Pair(v.dateView, NewsDetailsActivity.VIEW_NAME_DATE),
            Pair(v.imageView, NewsDetailsActivity.VIEW_NAME_IMAGE),
            Pair(v.card_view, NewsDetailsActivity.VIEW_NAME_LAYOUT)
        )
        intent.putExtra("text", news.text)
        intent.putExtra("date", DateHelper.formatNewsDate(news.date))
        intent.putExtra("imageLinks", news.images)
        intent.putExtra("title", news.title)

        ActivityCompat.startActivity(context!!, intent, options.toBundle())
    }

    private val loadingSection = LoadingSection()
    private val recyclerAdapter = SectionedRecyclerViewAdapter().apply {
        addSection(itemsSection)
        addSection(loadingSection)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = recyclerAdapter
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = linearLayoutManager
        viewModel = NewsListViewModel(NetworkService.getInstance())
        bindToModel()

        setupLoadWhileScrolling()
        viewModel.loadMoreAction(Unit)
    }

    private fun bindToModel() {
        val qwe1 = viewModel.loadMoreAction.errors.subscribe(::showError)
        val qwe2 = viewModel.items.subscribe(::updateSection)
        val qwe3 = viewModel.hasMoreToLoad.filter { !it }.subscribe { removeFooter() }
    }

    private fun setupLoadWhileScrolling() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val bottommostView = recyclerView.children.reduce { lastResult, child ->
                    if (child.top > lastResult.top) child else lastResult
                }
                val position = recyclerView.getChildAdapterPosition(bottommostView)
                if (position == recyclerAdapter.itemCount - 1) {
                    viewModel.loadMoreAction(Unit)
                }
            }
        })
    }

    private fun updateSection(items: List<News>) {
        val sectionAdapter = recyclerAdapter.getAdapterForSection(itemsSection)
        val needToScroll = itemsSection.contentItemsTotal == 0
        itemsSection.updateItems(items).dispatchUpdatesTo(sectionAdapter)
        if (needToScroll) recyclerView.scrollToPosition(0)
    }

    private fun removeFooter() {
        if (recyclerAdapter.getSectionIndex(loadingSection) == -1) return
        val sectionPositionInAdapter =
            recyclerAdapter.getAdapterForSection(loadingSection).sectionPosition
        recyclerAdapter.removeSection(loadingSection)
        recyclerAdapter.notifyItemRangeRemoved(
            sectionPositionInAdapter,
            loadingSection.contentItemsTotal
        )
    }

    companion object {
        fun newInstance() = NewsFragment()
    }

}
