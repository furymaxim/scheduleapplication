package com.furymaxim.schedulenew.ui.fragments

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {

    protected open fun showError(error: Throwable) {
        view?.let {
            Snackbar.make(
                it.rootView, error.localizedMessage
                    ?: error.javaClass.name, Snackbar.LENGTH_LONG
            ).show()
        }
    }
}