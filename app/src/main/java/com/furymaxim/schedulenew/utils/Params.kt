package com.furymaxim.schedulenew.utils

class Params {

    companion object {
        const val NEWS = "NEWS"
        const val NAME = "NAME"
        const val GROUP = "GROUP"
        const val LOGIN = "LOGIN"
    }
}