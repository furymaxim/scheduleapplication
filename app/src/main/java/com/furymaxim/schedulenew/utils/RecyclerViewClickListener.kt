package com.furymaxim.schedulenew.utils

import android.view.View

interface RecyclerViewClickListener {

    fun onClick(position: Int, view: View)
}