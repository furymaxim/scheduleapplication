package com.furymaxim.schedulenew.helpers

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import java.util.*


object MeasureHelper {

    fun getScrollByYPos(context: Context): Int {
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        val yearHeight = 50
        val monthHeight = 430

        val dipHeight =
            2 * yearHeight + monthHeight * 12 + currentMonth * monthHeight - monthHeight / 3

        val r: Resources = context.resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dipHeight.toFloat(),
            r.displayMetrics
        )

        return px.toInt()
    }
}