package com.furymaxim.schedulenew.helpers;

import com.furymaxim.schedulenew.models.MyDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateHelper {

    private static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private long diffInMillies;

    private static Date parseTimeUtcToDateCustomPattern(String time, String pattern) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(pattern, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {
        }
        return null;
    }

    public static Date parseTimeUtcToDate(String time) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(DEFAULT_FORMAT, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {

        }
        return null;
    }

    private static String formatDate(Date input, String pattern) {
        if (input == null) {
            return null;
        }
        try {
            SimpleDateFormat output = new SimpleDateFormat(pattern, Locale.getDefault());
            return output.format(input);
        } catch (Throwable ignored) {

        }
        return null;
    }

    public static String formatNewsDate(String input) {
        Date date = DateHelper.parseTimeUtcToDateCustomPattern(input, DEFAULT_FORMAT);
        return formatDate(date, "dd MMM yyyy");
    }

    public static String formatScheduleTitleDate(Date date) {
        return formatDate(date, "EEEE, dd MMMM yyyy г.");
    }

    public static MyDate getStartOfMonth(Calendar calendar) {
        String day = "1";
        String month = getCurrentMonth(calendar);
        String year = getCurrentYear(calendar);

        return new MyDate(day, month, year);
    }

    public static MyDate getEndOfMonth(Calendar calendar) {
        String day = String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        String month = getCurrentMonth(calendar);
        String year = getCurrentYear(calendar);

        return new MyDate(day, month, year);
    }

    public static String getCurrentDayOfMonth() {
        return String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
    }

    public static String getCurrentMonth(Calendar calendar) {
        return String.valueOf(calendar.get(Calendar.MONTH) + 1);
    }

    public static String getCurrentYear(Calendar calendar) {
        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }


}
