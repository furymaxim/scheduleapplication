package com.furymaxim.schedulenew.helpers;

public class StringHelper {

    public static String getTime(String string) {
        return string.substring(11, 16);
    }

    public static String getDay(String string) {
        return string.substring(8, 10);
    }

    public static String getMonth(String string) {
        return string.substring(5, 7);
    }

    public static String getYear(String string) {
        return string.substring(0, 4);
    }

}
